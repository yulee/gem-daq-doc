# Taking calibration scans & data

The scans and the analyses of the scans need to follow a specific naming convention: 

`<sbit-rate-scan|gbt-phase-scan|latency-scan|dac-scan>-<in|out>-[<comment>-]-<date+revision>`

Meaningfull scans that are supposed to be stored for future reference are to be stored in the path `/gemdata/data/` with the above naming convention.

## Taking an S-bit rate scan at P5

### Starting the S-bit rate scan

To take an S-bit scan, first connect to [RCMS](http://cmsrc-gem.cms:20000/rcms/gui/servlet/FMPilotServlet), then create a miniDAQ3 session or attach to a running session.

From the RCMS interface, click on "Initialize" and "Connect".

Consider that usually S-bits scans are taken without HV amplification to investigate the noise level in the detector, so you might need to take action with the DCS on the HV settings.

Open the [xDAQ](http://srv-s2g18-33-01.cms:20400/urn:xdaq-application:service=calibration) calibration suite. In case the calibration suite page was already opened before, reload the page manually.

From the dropdown menu, select the calibration type "S-bit ARM DAC scan", then  set the number of iterations in the "Iterations" text box to 6 and leave all the other parameters to their default values. Click on "Apply settings".  

move to the RCMS interface leaving the calibration web page open and  click "Configure". Then in the calibration web page, press "Run".

While the scan is ongoing, from any p5 GEM DAQ machine check that the output files are being written in `/gemdata/runs/minidaq/sBitRateScan`. You can also check in the logs present in `/gemdata/log/` what the software is doing.

The output files needs to be moved otherwise they will get overwritten by the next scan. 

### Analyzing the S-bit scan

To analyze an S-bit scan locate first the output in `/gemdata/runs/minidaq/sBitRateScan`. If these data are meant to be saved for later reference or analysis, copy the directory in the path `/gemdata/data/` with the naming convention:

`<sbit-rate-scan-in-[<comment>]-<data+revision>`

At these point analyze the scan using the command `gemos analyze sbit` from the terminal of `gem-locdaq02`: the path of the input file and output directory need to be specified, as well as the options for the plotting and allowed rate for the thresholds settings. 

## Taking a minidaq run with given threshold

To take a threshold scan, first connect to [RCMS](http://cmsrc-gem.cms:20000/rcms/gui/servlet/FMPilotServlet), then create a miniDAQ3 session or attach to a running session.

From the RCMS interface, click on "Initialize" and "Connect". Open the status table by the appropriate button and open the gem calibration interface in the list of running applications. 

**Enable the transfer to tier0 (T0) in the drop down window below the "DAQ" system column, if the data are intended to be saved to disk. It is the case if the data are to be given to the DPG for analysis.**

Select the "Physics" option in the drop down window. The option to use a given threshold for all VFATs instead of the one from the VFAT configuration can be chosen and the threshold can be set. Click on the "Apply settings" button.

Return to the RMCS interface, press on "Configure" and "Start". Once the statistics will be satisfactory, the run should be stopped with the "Stop" button. Then the miniDAQ instance can be destroyed with the "Destroy" button. 

## Taking a CSC-EMTF-GEM join run on the integration setup in building 904

This paragraph covers the required actions on the GEM side in order to be able to take cosmic data on the GE1/1 integration setup using as triggers the signals from the ME1/1 chamber, available in the setup.

As prerequisite, the GEM operator needs to have an account in the `cms904` network (linked to the `cms` network) and a developer account on the `gem904daq04` machine.
The operator needs to have the GEM DAQ software installed and working correctly, as per the instructions <https://gitlab.cern.ch/cmsgemonline/gem-daq/cmsgemos>.
Taking a test run is advisable.

1. The AMC13 needs to be connected to the b904 TCDS sub-system by connecting to its TTS/TTS input the TCDS yellow fiber with "B" on the top and "A" on the bottom.
2. Start the GEM xDAQ executive with the `b904int-ge11-join-runs` configuration.
```
CMSGEMOS_CONFIG_NAME=b904int-ge11-join-runs CMSGEMOS_PORT=20100 _build/_install/bin/gem-start-xdaq
```
3. Connect to the xDAQ GEM supevisor page and initialize the GEM FSM. Refer to the CSC DAQ expert (usually Karoly Banicz) to have the proper configuration of the CSC ME1/1 chamber.
4. The script `CSC_GEM_Local.sh` available in the `cms904` network, in the `/nfshome0/gempro/bin` folder, is used to trigger the state transitions is the proper order.
5. The cosmic run with the ME1/1 chamber providing the triggers, can be taken by using the above script to change, in the right order, the FSM of the GEM and the CSC.
```
/nfshome0/gempro/bin/CSC_GEM_Local.sh Configure
/nfshome0/gempro/bin/CSC_GEM_Local.sh Start
/nfshome0/gempro/bin/CSC_GEM_Local.sh Stop
```
6. The GEM raw data will be stored in the user's temporary folder, `$TMPDIR`, and overwritten at every run.
   Be careful to save the data somewhere if it is important to be kept.

