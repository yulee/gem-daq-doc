# Infrastructure at p5

The GEM infrastructure at p5 is partly managed by the CMS sysadmin team, and partly managed by the GEM DAQ group.
The sysadmins are responsible for computer hardware and ensuring software is able to be installed in an automatic (Puppetized) way.
They are also responsible for the network configuration.

The GEM DAQ experts are responsible for the GEM DAQ harware and updating software repository profiles in the software dropbox.

## Sysadmin support

Support from the sysadmin team can be sought several ways.
The on-call CMS DAQ expert can initiate support for issues related to the central DAQ infrastructure (e.g. miniDAQ).
For all other issues, the CMS sysadmins utilize a Jira ticketing system ([described here](https://twiki.cern.ch/twiki/bin/view/CMS/Net-and-Sys-admin-Users)).
To open a ticket you must be a member of the appropriate e-group and then use the [CMSONS Jira portal](https://its.cern.ch/jira/browse/CMSONS).

## Computers

GEM machines:

| Hostname                | Machine alias  | Notes                                    |
|-------------------------|----------------|------------------------------------------|
| `srv-s2g18-33-01`       | `gem-daq01`    | Data taking applications, PowerEdge R340 |
| `srv-s2g18-31-01`       | `gem-daq02`    | DAQ services, PowerEdge R340             |
| `srv-s2g18-34-01`       | `gem-locdaq01` | Local DAQ readout, PowerEdge R440        |
| `srv-s2g18-32-01`       | `gem-locdaq02` | Analysis suite, PowerEdge R440           |
| `kvm-s3562-1-ip151-107` | `gemvm-legacy` | Legacy software installation, VM         |
| `kvm-s3562-1-ip151-74`  | `gemvm-test`   | VM                                       |

Machines used by GEM:

| Hostname                | Machine alias               | Notes                                     |
|-------------------------|-----------------------------|-------------------------------------------|
| `ctrl-s2c17-22-01`      | `ctrlhub-gem-01`[^1]        | `sysmgr` and `control_hub` host           |
| `kvm-s3562-1-ip151-110` | `cmsrc-gem`, `gemrc-gemdev` | `RCMS` host (i.e., function managers)     |
| `bu-c2f13-16-01`        |                             | DAQ2 BU (access to the Lustre filesystem) |
| `fu-c2f11-23-03`        |                             | MiniDAQ DQM                               |

[^1]: Additional aliases: `bridge-s2e01-04`, `bridge-s2e01-14`, `bridge-s2e01-23`

### Remote powering

Instructions on remotely powering on/off the centrally managed GEM machines can be found on the P5 cluster users guide twiki [IPMI instructions](https://twiki.cern.ch/twiki/bin/view/CMS/ClusterUsersGuide#IPMI).

!!! warning
    These on/off actions should **only** be taken after confirmation that it is OK from the CMS Technical Coordination and CMS sysadmins.

### Software dropbox

Information on using the `dropbox2` tool to update the software
associated with the puppetized machines can be found on the P5 cluster
users guide twiki [dropbox
link](https://twiki.cern.ch/twiki/bin/view/CMS/ClusterUsersGuide#How_to_use_the_dropbox_computer).

!!! note
    If a package has not previously been included in a GEM puppet profile, a
    [Jira ticket](https://its.cern.ch/jira/projects/CMSONS) should be
    opened, providing the initial RPM. Following this, updating the RPM is
    done in the normal way with the `dropbox2` tool.

### Monitoring (icinga)

Monitoring of central infrastructure is provided via `icinga` ([GPN](http://cmsicinga2.cern.ch/icinga-web/)) or ([P5](http://cmsicinga2.cms/icinga-web/)).
SMS notifications are sent to a list of CERN mobile numbers, the DAQ expert phone is one.
Email notifications are sent to the members of the `cms-gem-online-notifications` e-group via the `cms-gem-daq-notifications` email address.
The notification polices can be changed by opening a [Jira ticket](https://its.cern.ch/jira/projects/CMSONS), using any configuration defined in [this documentation](https://docs.icinga.com/latest/en/notifications.html).

## DAQ hardware

### Racks

#### S2E01

| Device            | Notes                                              |
|-------------------|----------------------------------------------------|
| `psu-s2e01-24-01` | GE1/1 Aspiro PSU                                   |
| `mch-s2e01-23-01` | GE-1/1 µTCA crate - FED1467 (GEM-)                 |
| `mch-s2e01-14-01` | GE+1/1 µTCA crate - FED1468 (GEM+)                 |
| `psu-s2e01-05-01` | GE2/1 Aspiro PSU                                   |
| `mch-s2e01-04-01` | GE2/1 demonstrator µTCA crate - FED1469 (GEMPILOT) |
