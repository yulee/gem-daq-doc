# CMS GEM DAQ Documentation

Prepare your environment:

``` bash
python3 -m venv pyvenv
source pyvenv/bin/activate
python3 -m pip install --upgrade pip
python3 -m pip install -r requirements.txt
deactivate
```

Start a local web server to visualize your changes:

``` bash
pyvenv/bin/mkdocs serve
```
